const express = require('express');
const http = require('http');
const socketIo = require('socket.io');
const mongoose = require('mongoose');

const index = require('./routes/index');


const args = process.argv.slice(2)
let dev = args.length > 0 && args[0] === "dev";

// SETUP MONGOOSE
let mongoUrl = `mongodb://${dev ? 'localhost' : 'mongodb'}:27017/lappis-test`;
let connectWithRetry = () => {
    return mongoose.connect(mongoUrl, { useNewUrlParser: true }, (err) => {
        if (err) {
            let timeout = 5000;
            console.error("Failed to connect to mongo - retrying in " + timeout / 1000 + " seconds", err)
            setTimeout(connectWithRetry, 5000)
        }
        else {
            console.log("Connected to mongo!")
        }
    })
}

connectWithRetry();
mongoose.set('useFindAndModify', false);



const noteSchema = new mongoose.Schema({
    author: { name: String, id: String },
    text: String,
    date: { type: Date, default: Date.now },
    position: { type: [Number], default: [] },
    color: { type: String, default: "#fff9c4" },
    upvotes: { type: [String], default: [] },
    downvotes: { type: [String], default: [] },
    public: { type: Boolean, default: false }
});

const roomSchema = new mongoose.Schema({
    name: String,
    notes: [noteSchema]
});

//------- TODO -------\\
// ADD MEETING LOGIC
let Room = mongoose.model('Room', roomSchema);

let Note = mongoose.model('Note', noteSchema);

// SETUP SOCKET.IO
const port = process.env.PORT || 4001;

const app = express();
app.use(index);

const server = http.createServer(app);

const io = socketIo(server);

const emitNotesToTarget = (room, target) => {
    getNotesFromRoom(room, (err, data) => {
        emitNotesToAuthor(data, target)
    });
};

// PRIVATE NOTES SHOULD BE SEND TO CORRECT CLIENT

const emitNotesToRoom = async (room, notes) => {
    let clients = await Object.keys(io.sockets.adapter.rooms[room].sockets)
    clients.forEach(client => {
        emitNotesToAuthor(notes, io.sockets.connected[client]);
    })
};

const emitNotesToAuthor = (notes, socket) => {
    let result = notes.filter((n) => n.public || n.author.id === socket.authorId);
    socket.emit("notes", result);
}

const getNotesFromRoom = (room, callback = (err, data) => emitNotesToRoom(room.name, data)) => {
    Room.find({ name: room.name }, (err, result) => callback(err, result && result[0].notes));
}

const addNoteToRoom = (roomName, note, callback = (err, data) => emitNotesToRoom(roomName, data.notes)) => {
    Room.findOneAndUpdate(
        { name: roomName },
        { $push: { notes: note } },
        (err, result) => callback(err, result)
    );
}

const updateNoteInRoom = (roomName, note, callback = (err, data) => emitNotesToRoom(roomName, data.notes)) => {
    let set = {};
    Object.keys(note).forEach((curr) => {
        set['notes.$.' + curr] = note[curr];
    })
    if (!note.position) {
        set['notes.$.date'] = Date.now();
    }
    Room.findOneAndUpdate({ name: roomName, "notes._id": note._id }, set, (err, result) => callback(err, result));
}

const removeNoteFromRoom = (roomName, noteId, callback = (err, data) => emitNotesToRoom(roomName, data.notes)) => {
    Room.findOneAndUpdate({ name: roomName }, { "$pull": { notes: { _id: noteId } } }, (err, result) => callback(err, result));
}

const getRoom = (name, callback) => {
    Room.findOneAndUpdate({ name }, { name }, { upsert: true, new: true }, callback);
}

const upvote = (roomName, noteId, authorId, callback) => {
    let operation = {
        '$addToSet': {
            'notes.$.upvotes': authorId

        },
        '$pull': {
            'notes.$.downvotes': authorId

        }
    };
    Room.findOneAndUpdate(
        { name: roomName, "notes._id": noteId },
        operation,
        (err, result) => callback(err, result)
    );
}

const downvote = (roomName, noteId, authorId, callback) => {
    let operation = {
        '$addToSet': {
            'notes.$.downvotes': authorId

        },
        '$pull': {
            'notes.$.upvotes': authorId

        }
    };
    Room.findOneAndUpdate({ name: roomName, "notes._id": noteId }, operation, (err, result) => callback(err, result));
}


io.on("connection", socket => {
    console.log("New client connected");
    let room;

    socket.emit("connection", 'connected');

    socket.on('join-room-request', (data => {
        getRoom(data.room, (err, result) => {
            room = result;
            socket.join(room.name);
            socket.authorId = data.authorId;
            console.log("New client in", room.name);
            socket.emit("joined-room", room.name);
        })
    }));

    socket.on("submit-note", (data) => {
        addNoteToRoom(room.name, data, (err, result) => getNotesFromRoom(room));
    });

    socket.on("upvote", ({ authorId, noteId }) => {
        upvote(room.name, noteId, authorId, (err, result) => getNotesFromRoom(room));
    });

    socket.on("downvote", ({ authorId, noteId }) => {
        downvote(room.name, noteId, authorId, (err, result) => getNotesFromRoom(room));
    });

    socket.on("remove-note", (id) => {
        removeNoteFromRoom(room.name, id, (err, result) => getNotesFromRoom(room));
    })

    socket.on("edit-note", (data) => {
        updateNoteInRoom(room.name, data, (err, result) => getNotesFromRoom(room));
    });

    socket.on("move-note", (data) => {
        updateNoteInRoom(room.name, data, (err, result) => getNotesFromRoom(room));
    });

    socket.on("get-notes", () => emitNotesToTarget(room, socket));

    socket.on("disconnect", () => console.log("Client disconnected"));
})

server.listen(port, () => console.log(`Listening on port ${port}`));