const express = require('express');

const router = express.Router();

router.get("/", (req, res) => {
    res.send({ response: "I am alive" }).status(200);
});

router.get("/api", (req, res) => {
    res.send({ response: "api could be reached" }).status(200);
})

module.exports = router;