import { createGlobalStyle } from 'styled-components';

const font = "'Century Gothic', CenturyGothic, AppleGothic, Arial, Helvetica, sans-serif";

const GlobalStyle = createGlobalStyle`
    body {
        margin: 0;
        padding: 0;
    }

    #App {
        text-align: center;
        font-family: ${font};

        input, button, textarea, select {
            font-family: ${font};
        }
    }
`;

export default GlobalStyle;