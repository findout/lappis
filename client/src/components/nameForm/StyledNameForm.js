import styled from 'styled-components';

const StyledNameForm = styled.div`
    display: ${props => props.visible ? "block" : "none"};

    background-color: rgba(0, 0, 0, 0.6);
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 20000;
    backdrop-filter: blur(10px);

    .input-box {
        min-width: 200px;
        min-height: 200px;
        width: 50%;
        height: 60%;
        background-color: white;
        display: flex;
        justify-content: center;
        margin: 50px auto;

        .title {
            margin-top: 80px;
        }

        .name-input {

        }
    }
`;

export default StyledNameForm;