import React, { useState, useEffect } from 'react';
import moment from 'moment';
import ReactTooltip from 'react-tooltip';
import Textarea from 'react-textarea-autosize';
import { GithubPicker } from 'react-color';
import { FaEyeDropper, FaTrash, FaThumbsUp, FaThumbsDown } from 'react-icons/fa'

import StyledNote from './note/StyledNote';
import DragSource from 'components/DragSource';

const Note = ({ author, upvotes, downvotes, color, text, date, position, onMove, onDelete, onVote, onColorChange, onChange: onTextChange, ...miscProps }) => {
    const [x, setX] = useState(position[0]);
    const [y, setY] = useState(position[1]);
    const [message, setMessage] = useState(text);
    const [showColorPicker, setShowColorPicker] = useState(false);
    const [showPopups, setShowPopups] = useState(true);

    useEffect(() => {
        document.addEventListener("mouseup", handleClickOutside);

        return () => {
            document.removeEventListener("mouseup", handleClickOutside)
        }
    }, [])

    useEffect(() => {
        setX(position[0]);
        setY(position[1]);
    }, [position])

    useEffect(() => {
        setMessage(text)
    }, [text])

    const togglePopups = () => setShowPopups(!showPopups)

    const onEnterPress = e => {
        if (e.charCode === 13 && e.shiftKey === false) {
            e.preventDefault();
            e.target.blur();
            onTextChange(message);
        }
    }

    const handleColorChange = c => {
        setShowColorPicker(false);
        onColorChange(c);
    }

    const handleClickOutside = () => {
        setShowColorPicker(false);
    }

    const getScore = () => {
        const score = upvotes.length - downvotes.length
        return score;
    }

    return (
        <StyledNote
            {...miscProps}
            x={x}
            y={y}
            data-tip={`${author && author.name} ${date && moment(date).fromNow()}`}
            data-for={`${miscProps._id}-tooltip`}
            color={color}
            showColorPicker={showColorPicker}
        >

            {showPopups &&
                <ReactTooltip place="top" effect="solid" className="tooltip" id={`${miscProps._id}-tooltip`} />
            }
            <DragSource x={x} y={y} id={miscProps._id} togglePopups={togglePopups} >
                <div className="main">
                    <div className="controls top">
                        <div className="vote-controls">
                            <FaThumbsUp size="0.8em" className="control-button" color="green" onClick={() => onVote(1)} />
                            <FaThumbsDown size="0.8em" className="control-button" color="red" onClick={() => onVote(-1)} />
                        </div>
                    </div>
                    <div className="content">
                        <Textarea
                            className="text"
                            value={message}
                            onChange={(e) => setMessage(e.target.value)}
                            onKeyPress={onEnterPress}
                            minRows={2}
                            maxRows={10}
                        />
                    </div>
                    <div className="controls bottom">
                        <FaEyeDropper size="0.8em" className="color-picker-button control-button" onClick={() => setShowColorPicker(true)} />
                        <span className="score">{getScore()}</span>
                        <FaTrash size="0.8em" className="trash-button control-button" onClick={onDelete} />
                    </div>
                </div>


                <div className="color-picker">
                    <GithubPicker
                        color={color}
                        onChangeComplete={c => handleColorChange(c.hex)}
                        colors={[
                            "#ffcdd2", "#f8bbd0", "#e1bee7", "#d1c4e9", "#c5cae9", "#bbdefb", "#b3e5fc", "#b2ebf2",
                            "#b2dfdb", "#c8e6c9", "#dcedc8", "#f0f4c3", "#FFF9C4", "#FFECB3", "#ffe0b2", "#ffccbc"
                        ]}
                    />
                </div>
            </DragSource>

        </StyledNote>
    );
}

export default Note;