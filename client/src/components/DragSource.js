import React from 'react'
import { DragSource } from 'react-dnd'
import { ITEM } from 'modules/ItemTypes'


const Source = ({ connectDragSource, isDragging, children }) => (
    !isDragging && connectDragSource(<div>{children}</div>)
)

const spec = {
    beginDrag(props) {
        const { x, y, id, togglePopups } = props
        if (togglePopups) togglePopups()
        return { x, y, id }
    },
    endDrag(props, monitor) {
        const { togglePopups } = props
        if (togglePopups) togglePopups()
        if (!monitor.didDrop()) return
    }
}

const collect = (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
})

export default DragSource(ITEM, spec, collect)(Source)