import styled from 'styled-components';

const StyledNote = styled.div`
    position: absolute;

    left: ${props => props.x}px;
    top: ${props => props.y}px;

    z-index: ${props => props.priority + 10 || 10};

    width: 100px;

    .main {
        
        box-shadow: 2px 2px 2pt 2px rgba(0, 0, 0, 0.2);
        padding-top: 10px;
        display: flex;
        flex-direction: column;
        justify-content: space-around;
        align-items: center;
        background-color: ${props => props.color};
        position: relative;
    
        .content {
            font-size: 10pt;
            flex-grow: 1;
            width: 100%;
            margin-top: 10px;

            .text {
                resize: none;
                background-color: inherit;
                text-align: center;
                width: 100%;
                height: 100%;
                box-sizing: border-box;
                padding: 0 5px;
                outline: none;
                border: none;
                box-shadow: none;
                font-size: 9pt;
                overflow: auto;
            }
        }

        .controls {
            opacity: 0;
            transition: opacity 0.1s;

            .control-button:hover {
                opacity: 0.6;
            }

            &.bottom {
                width: 100%;
                padding: 0 5px;
                box-sizing: border-box;
                display: flex;
                justify-content: space-between;
                align-items: center;

                .score {
                    font-size: 1em;
                }
            }

            &.top {
                position: absolute;
                top: 5px;
                left: 5px;
                right: 5px;

                .vote-controls {
                    display: flex;
                    justify-content: space-between;

                    & > * {
                        margin: 3px;
                    }
                }
            }
        }

        &:hover .controls {
            opacity: 1;
        }
    }

    .color-picker {
        display: ${props => props.showColorPicker ? "block" : "none"};
        margin-top: 10px;
    }

    .tooltip {
        z-index: 9999;
    }
`;

export default StyledNote;