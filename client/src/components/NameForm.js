import React, { useState } from 'react';

import StyledNameForm from './nameForm/StyledNameForm';

const NameForm = ({ visible, onSubmit }) => {
    const [name, setName] = useState("");

    const onFormSubmit = (e) => {
        e.preventDefault();
        onSubmit(name);
    }

    return (
        <StyledNameForm visible={visible}>
            <div className="input-box">
                <form onSubmit={onFormSubmit}>
                    <label>
                        <h2 className="title">Enter your name:</h2>
                        <input
                            className="name-input"
                            type="text"
                            value={name}
                            onChange={e => setName(e.target.value)}
                        />
                    </label>
                    <input type="submit" value="Submit" />
                </form>
            </div>
        </StyledNameForm>
    );
}

export default NameForm;