import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { CookiesProvider } from 'react-cookie';
import { DragDropContext } from 'react-dnd';
import MultiBackend from 'react-dnd-multi-backend';
import HTML5toTouch from 'react-dnd-multi-backend/lib/HTML5toTouch'

import Meeting from 'containers/Meeting';
import GlobalStyle from 'GlobalStyle';

const App = () => {
  return (
    <div id="App">
      <GlobalStyle />
      <CookiesProvider>
        <Router>
          <Route exact path="/" component={Meeting} />
          <Route path="/:room" component={Meeting} />
        </Router>
      </CookiesProvider>
    </div>
  )
}

export default DragDropContext(MultiBackend(HTML5toTouch))(App);
