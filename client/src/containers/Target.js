import { DropTarget } from 'react-dnd'
import { ITEM } from 'modules/ItemTypes'

const Target = ({ connectDropTarget, children }) => (
    connectDropTarget(children)
)

const spec = {
    drop(props, monitor) {

        const { onDrop, isPublic } = props

        const item = monitor.getItem()
        const delta = monitor.getDifferenceFromInitialOffset()
        const left = Math.round(item.x + delta.x)
        const top = Math.round(item.y + delta.y)
        onDrop(item.id, left, top, isPublic)
    }
}

const collect = (connect) => ({
    connectDropTarget: connect.dropTarget()
})

export default DropTarget(ITEM, spec, collect)(Target)