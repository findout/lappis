import React, { useEffect, useState, useRef } from 'react';
import socketIOClient from 'socket.io-client';
import _ from 'underscore';
import uniqid from "uniqid";
import { useCookies } from 'react-cookie';

import StyledMeeting from 'containers/meeting/StyledMeeting';
import Note from 'components/Note';
import NameForm from '../components/NameForm';
import Target from 'containers/Target';

const Meeting = ({ match: { params: { room } }, history }) => {
    const [endpoint] = useState(`http://${window.REACT_APP_API_HOST ? window.REACT_APP_API_HOST : "localhost:4001"}`);
    const [socket, setSocket] = useState(undefined);
    const [noteList, setNoteList] = useState([]);
    const [focusPrio, setFocusPrio] = useState([]);
    const [height, setHeight] = useState(undefined);
    const [width, setWidth] = useState(undefined);

    const privateAreaRef = useRef(undefined);
    const authorIdRef = useRef(undefined);

    const [cookies, setCookie] = useCookies(['author', 'id']);

    useEffect(() => {
        console.log("connecting to", endpoint);
        authorIdRef.current = cookies.id || uniqid();

        const socket = socketIOClient(endpoint);

        socket.on("connection", () => socket.emit("join-room-request", { room: room || uniqid(), authorId: authorIdRef.current }));
        socket.on("joined-room", roomName => {
            history.push('/' + roomName);
            socket.emit("get-notes");
        })
        socket.on("notes", data => setNoteList(data ? data : []));
        socket.on("response", data => console.log("response:", data))

        setSocket(socket);

        return () => {
            socket.removeAllListeners();
        }
    }, [])

    useEffect(() => {
        let maxX = _.reduce(noteList, (prev, { position: curr }) => curr[0] > prev ? curr[0] : prev, 0);
        let maxY = _.reduce(noteList, (prev, { position: curr }) => curr[1] > prev ? curr[1] : prev, 0);
        setWidth(maxX + 100);
        setHeight(maxY + 100);
    }, [noteList])

    useEffect(() => {
        setCookie('id', authorIdRef.current);
    }, [authorIdRef.current])

    const setAuthorCookie = (author) => {
        setCookie('author', author)
    }

    const submitNote = (note) => {
        socket.emit("submit-note", note);
    }

    const onNoteMoved = (id, x, y, notePublic = true) => {
        let newNotes = [...noteList]
        let index = newNotes.findIndex((curr) => curr._id === id)
        newNotes[index] = { ...newNotes[index], position: [x, y] }
        setNoteList(newNotes)
        socket.emit("edit-note", {
            _id: id,
            position: [x, y],
            public: notePublic,
            ...(!notePublic && { author: { id: cookies.id, name: cookies.author } })
        });
    }

    const onNoteChanged = (id, message) => {
        socket.emit("edit-note", { _id: id, text: message, author: { id: cookies.id, name: cookies.author } });
    }

    const onColorChanged = (id, color) => {
        socket.emit("edit-note", { _id: id, color });
    }

    const moveToTop = (id) => {
        let newFocus = [...focusPrio];
        if (_.includes(newFocus, id)) {
            newFocus.splice(newFocus.indexOf(id), 1)
        }
        newFocus.push(id);
        setFocusPrio(newFocus);
    }

    const createPrivateNote = () => {
        const offset = privateAreaRef.current.getBoundingClientRect();
        const position = [
            offset.x + 20,
            offset.y + 100
        ]
        if (offset.height < 200) position[1] -= 200 - offset.height
        if (offset.width < 120) position[0] -= 120 - offset.width
        let note = { author: { id: cookies.id, name: cookies.author }, text: "", position }
        submitNote(note);
    }

    const upvote = (id) => {
        socket.emit("upvote", { authorId: cookies.id, noteId: id });
    }

    const downvote = (id) => {
        socket.emit("downvote", { authorId: cookies.id, noteId: id });
    }

    const onVote = (score, id) => {
        score > 0 ? upvote(id) : downvote(id);
    }

    const removeNote = (id) => {
        socket.emit("remove-note", id);
    }

    const filterNotes = (filter) => {
        return _.map(noteList.filter(filter), (note) => {
            return (
                <Note
                    {...note}
                    key={note._id}
                    color={note.color}
                    onMouseDown={() => moveToTop(note._id)}
                    priority={focusPrio.indexOf(note._id)}
                    onMove={(x, y) => onNoteMoved(note._id, x, y)}
                    onChange={(message) => onNoteChanged(note._id, message)}
                    onColorChange={color => onColorChanged(note._id, color)}
                    onVote={(score) => onVote(score, note._id)}
                    onDelete={() => removeNote(note._id)}
                />
            );
        });
    }

    return (
        <StyledMeeting width={width} height={height}>
            <NameForm visible={!cookies.author} onSubmit={setAuthorCookie} />
            <Target onDrop={onNoteMoved} isPublic={false}>
                <ul className="private"
                    ref={privateAreaRef}
                >
                    <h2>Private Notes</h2>
                    <div className="input">
                        <button onClick={createPrivateNote}>Add Note</button>
                    </div>
                    {
                        filterNotes(n => !n.public)
                    }
                </ul>
            </Target>
            <Target onDrop={onNoteMoved} isPublic>
                <ul className="public"
                >
                    <h2>Public Notes</h2>
                    {
                        filterNotes(n => n.public)
                    }
                </ul>
            </Target>
        </StyledMeeting>
    );
};

export default Meeting;