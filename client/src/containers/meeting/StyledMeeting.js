import styled from 'styled-components';

const StyledMeeting = styled.div`
    display: grid;
    grid-template-areas:
        ". header header"
        ". private public";
    grid-template-columns: 20px 150px 3fr;
    min-width: 100vw;
    width: ${props => props.width}px;

    @media (pointer: coarse) {
        grid-template-areas:
            "private private private"
            "public public public";
    }

    .input {
        grid-area: header;
        height: 10vh;
    }

    .public {
        grid-area: public;
        margin: 0;
        padding: 0;
        height: ${props => props.height}px;
        min-height: 90vh;
    }

    .private {
        grid-area: private;
        height: ${props => props.height}px;
        box-shadow: 2px 2px 2pt 2px rgba(0, 0, 0, 0.2);
        background-color: #eee;
        margin: 0;
        padding: 0;

        @media (pointer: fine) {
            min-height: 90vh;
        }

        @media (pointer: coarse) {
            min-height: 200px;
            height: 20vh;
        }
    }
`;

export default StyledMeeting;