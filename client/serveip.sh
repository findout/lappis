#!/bin/bash
API_HOST="$(wget http://ipecho.net/plain -O - -q):8000"
if [ ! -z ${API_HOST} ]; then
cat <<END
window.REACT_APP_API_HOST="${API_HOST}";
END
fi