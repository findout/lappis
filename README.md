# Getting Started
## Prerequisites
### npm & Node
#### macOs
Install nodejs and npm

    brew install node

#### Ubuntu
Update packages list

    sudo apt update

Install nodejs

    sudo apt install nodejs npm

Install npm

    sudo apt install npm


### Yarn
#### macOs:
    brew install yarn

or 

    sudo port install yarn

#### Ubuntu
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -

    echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

Followed with

    sudo apt-get update && sudo apt-get install yarn

### Docker & Docker Compose (For Deploying)
[Installation instructions ](https://github.com/Yelp/docker-compose/blob/master/docs/install.md)

### MongoDB (For Running Locally)
#### macOS
    brew update
    brew install mongodb

Make "db" directory. Default is:

    mkdir -p /data/db

Make sure "db" directory has the right permissions

    > sudo chown -R `id -un` /data/db
    > # Enter your password

#### Ubuntu
Install the MongoDB packages

    sudo apt update
    sudo apt install -y mongodb

Verify that Active status is equal to `active (running)`

    sudo systemctl status mongodb

Start the MongoDB service

    sudo systemctl start mongodb

Configure the MongoDB to start on server start

    sudo systemctl enable mongodb

## Install Dependencies
In both /client and /server directories, run

    yarn

## Running locally
In /client directory, run

    yarn start

This will give an output that tells you which port the client server listens to.

In /server directory, run

    node App.js dev

Visit site at http://localhost:your-client-port

## Deploying
Make sure that the proper ports are open. The default ports for this project are 8000 for the backend server and 8001 for the client server. This can be changed with the .env file in the root directory of the project.


In the root directory of the project, run

    docker-compose up --build

To shut the app down, run

    docker-compose down

On the machine running the servers, run

    curl https://ipinfo.io/ip

The output will tell you where the app can be reached. To use the app, navigate in a browser to http://your-machine-ip:8001. The 